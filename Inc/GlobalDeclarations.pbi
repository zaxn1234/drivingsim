﻿Global Recul = #False 

Global MaxEngineForce.f = 3000.0 ;2000
Global MaxEngineBrake.f = 150.0

Global SteeringIncrement.f = 0.5
Global SteeringClamp.f = 27     

Global WheelRadius.f = 0.5;
Global WheelWidth.f = 0.4 ;

Global SuspensionStiffness.f = 20.0
Global SuspensionDamping.f = 3.3
Global SuspensionCompression.f = 4.4
Global MaxSuspensionTravelCm.f = 500.0;
Global FrictionSlip.f = 20		

Global RollInfluence.f = 0.3
Global SuspensionRestLength.f = 0.6;

Global Vehicle.s_Vehicle

Global Quit, ElapsedTime
Global showStatsFlag = 1 ;1 to start with stats open, -1 to start hidden
; IDE Options = PureBasic 5.72 (Windows - x64)
; CursorPosition = 23
; EnableXP