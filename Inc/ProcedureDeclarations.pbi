﻿Declare BuildVehicle(*Vehicle.s_Vehicle)
Declare HandleVehicle()
Declare ControlVehicle(elapsedTime.f)
Declare.f  Interpolation(x1.f, x2.f, percent.f)
Declare Clamp(*var.float, min.f, max.f)
Declare ShowStats()
; IDE Options = PureBasic 5.72 (Windows - x64)
; CursorPosition = 5
; EnableXP