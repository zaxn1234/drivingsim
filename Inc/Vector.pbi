﻿Macro VECTOR3(V, a, b, c)
  V\x = a
  V\y = b
  V\z = c
EndMacro

Structure Vector3
  x.f
  y.f
  z.f
EndStructure
; IDE Options = PureBasic 5.72 (Windows - x64)
; CursorPosition = 10
; Folding = -
; EnableXP