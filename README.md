# README #

## What is this repository for? ##

* This repository contains the source code for a WIP driving simulation game
* Latest version - 0.0.0

## How do I get set up? ##

* Summary
  - Download the latest release ZIP
  - Extract ZIP to desired location (ideally not somewhere administrator rights are required)
  - Run the `DrivingSim.exe` file
* Configuration
  - All configuration settings available to the user can be found in the .prefs file within the game folder
  - Alternatively, you can use the in-game menus to configure the options
* Dependencies
  - DX9+
  - Currently only available on Windows (tested on Win10 build 19043.1110) but MacOS/Linux ports will be available eventually

## Who do I talk to? ##

* Repo owner or admin
If you have any questions, email me [here](mailto:izaak.webster@ntlworld.com)